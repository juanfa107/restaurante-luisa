// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDja9SmlzHiMlihPAvG5uSlglFOwco3S5E",
    authDomain: "restaurante-a8130.firebaseapp.com",
    databaseURL: "https://restaurante-a8130.firebaseio.com",
    projectId: "restaurante-a8130",
    storageBucket: "restaurante-a8130.appspot.com",
    messagingSenderId: "605062022682",
    appId: "1:605062022682:web:17459bc2a8e23e876633a3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
