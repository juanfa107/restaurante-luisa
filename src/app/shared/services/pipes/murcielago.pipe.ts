import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'murcielago'
})
export class MurcielagoPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    let texto: string = '';
    const array = [ 'm','u','r','c','i','e','l','a','g','o'];
    const arrayMayus = ['M','U','R','C','I','E','L','A','G','O'];

    value = value.split('');

    value.forEach(e => {
        let aux = e;
        const pos = array.indexOf(e);
        const pos2 = arrayMayus.indexOf(e);

        if( pos != -1){
            aux = pos + 1;
        }else if(pos2 != -1){
            aux = pos2 + 1;
        }
        texto += aux.toString();
    });

    return texto;
  }

}