import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';


@Injectable({
  providedIn: 'root'
})
export class CarritoService {

  productos: Array<any> = [];

  private cantidad = new Subject<number>();
  public countdownEnd$ = this.cantidad.asObservable();

  private listaProductos = new Subject<any>();
  public obsListaProductos = this.listaProductos.asObservable();

  constructor(private _snackBar: MatSnackBar) {
  }

  agregar(elemento: object ){
    this.productos.push(elemento);
    this.cantidad.next(this.productos.length);
    this.listaProductos.next(this.productos);
    this.openSnackBar(`${elemento['name']} Agregado` );
  }


  openSnackBar(message: string, action: string = 'Agregado') {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  accion(tipo: string, item: string){

    const pos = this.productos.indexOf(item);
    switch(tipo){
        case 'aumentar':
         const elem = this.productos[pos];
         this.agregar(elem);
         break;
        case 'reducir':
            this.productos.splice(pos,1);
            this.cantidad.next(this.productos.length);
            this.listaProductos.next(this.productos);
         break;
    }

  }

}
