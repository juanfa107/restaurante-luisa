import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import {MatInputModule} from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon'
import {MatDatepickerModule, MatNativeDateModule, MatBadgeModule, MatSnackBarModule } from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AngularFireModule } from "@angular/fire";
import { environment } from 'src/environments/environment';
import { GetProductosService } from './shared/services/get-productos.service';
import { HttpClientModule } from '@angular/common/http';
import { CalculadoraComponent } from './components/calculadora/calculadora.component';
import { MurcielagoPipe } from './shared/services/pipes/murcielago.pipe';
import { CarritoComponent } from './components/products/carrito/carrito.component';
import { PedidoComponent } from './components/pedido/pedido.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    ProductsComponent,
    OrdersComponent,
    CalculadoraComponent,
    MurcielagoPipe,
    CarritoComponent,
    PedidoComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSelectModule,
    MatExpansionModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTooltipModule,
    AngularFireModule.initializeApp(environment.firebase),
    HttpClientModule,
    MatBadgeModule,
    MatSnackBarModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
