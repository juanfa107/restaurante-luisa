import { Component, OnInit } from '@angular/core';
import { CarritoService } from 'src/app/shared/services/carrito.service';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.scss']
})
export class PedidoComponent implements OnInit {
  lista: any = [];
  total: any;

  constructor(private carritoService: CarritoService) {
    this.listar();
  }

  ngOnInit() {

  }

  listar(){
    this.carritoService.obsListaProductos.subscribe(
      result =>{
        this.ordenarLista(result);
      }, error => {
        console.log('error: ', error);
      }
    )
  }

  ordenarLista(array){
    const listaOrdenada = [];

    array.forEach(element => {
      const pos = listaOrdenada.indexOf(element);
      if ( pos === -1 ) {
        element.cantidad = 1;
        listaOrdenada.push(element);
      } else {
        listaOrdenada[pos].cantidad +=1;
      }
    });

    this.lista = listaOrdenada;
    this.total = this.calcularTotal();

  }

  accion(tipo: string, pos: number){
    this.carritoService.accion(tipo, this.lista[pos]);
  }

  calcularTotal(){
    let total = 0;
    this.lista.forEach(element => {
      total += (parseInt(element.price) * element.cantidad );
    });

    return total;
  }

}
