import { Component, OnInit } from '@angular/core';
import { GetProductosService } from 'src/app/shared/services/get-productos.service';
import { CarritoService } from 'src/app/shared/services/carrito.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  constructor(
    private carrito: CarritoService
  ) {
    
  }

  products = [
    {
      name: 'Bandeja Paisa',
      description: 'descripcion arroz con pollo',
      price: '15000',
      img: './../../assets/img/comida/bandejaPaisa.jpg'
    },
    {
      name: 'Arepas',
      description: 'descripciona arepas',
      price: '5000',
      img: './../../assets/img/comida/arepas.jpg'
    },
    {
      name: 'Arepas Con Guiso',
      description: 'descripcion arepas con guiso',
      price: '7000',
      img: './../../assets/img/comida/arepaGuisp.jpg'
    },
    {
      name: 'Arroz Con Pollo',
      description: 'descripcion arroz con pollo',
      price: '12000',
      img: './../../assets/img/comida/arrozConPollo.jpg'
    },
    {
      name: 'Empanadas',
      description: 'descripcion empanadas',
      price: '2000',
      img: './../../assets/img/comida/empanadas.jpeg'
    },
    {
      name: 'Mojarra',
      description: 'descripcion mojarra',
      price: '17000',
      img: './../../assets/img/comida/mojarra.jpg'
    }
  ];

  ngOnInit() {
  }


  agregar(producto){
    this.carrito.agregar(producto);
  }
}
