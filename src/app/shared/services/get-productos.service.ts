import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetProductosService {
  api = 'http://apirestaurant.megainnovacion.com/';
  constructor(private http: HttpClient) {
    console.log('servicio iniciado');
  }

  getProductos(){
    return this.http.get('http://apirestaurant.megainnovacion.com/GetProductos');
  }

  query(route: string){
    return this.api + route;
  }


}
