import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.component.html',
  styleUrls: ['./calculadora.component.scss']
})
export class CalculadoraComponent implements OnInit {

  formulario;

  operaciones : object[] = [
    { nombre: 'sumar', simbolo: '+'},
    { nombre: 'restar', simbolo: '-'},
    { nombre: 'multiplicar', simbolo: '*'},
    { nombre: 'dividir', simbolo: '/'}
  ];

  sumar = (x,y) => x + y;
  restar = (x,y) => x - y;
  multiplicar = (x,y) => x * y;
  dividir = (x,y) => y!= 0 ?  (x / y) : 'No se puede dividir entre cero';
  resultado: any;

  constructor(
    private formBuilder: FormBuilder
  ) { 
    this.initForm();
  }

  ngOnInit() {
  }


  initForm(){
    this.formulario = this.formBuilder.group({
      numero1: 0,
      numero2: 0,
      operacion: '',
    });
  }

  resetear(){
    this.formulario.reset();
    this.formulario.get('numero1').setValue(0);
    this.formulario.get('numero2').setValue(0);
  }

  calcular(){
    console.log('entró');
    const valores = this.formulario.value;
    this.resultado = this[valores.operacion](valores.numero1, valores.numero2);
  }

}
