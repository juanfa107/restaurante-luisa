import { Component, OnInit } from '@angular/core';
import { CarritoService } from 'src/app/shared/services/carrito.service';

@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {
  cantidad = 0;
  constructor(
    private carritoService: CarritoService
  ) { 
    this.carritoService.countdownEnd$.subscribe(result => {
      this.cantidad = result;
    }, error => {
      console.log('error: ', error)
    });
  }

  ngOnInit() {
  }

}
