import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder 
  ) { 
    this.initForm();
  }

  step = 0;
  formulario;

  products: object[] = [
    {
      name: 'Bandeja Paisa',
      description: 'lorem ipsum',
      price: '$15000',
      img: './../../assets/img/comida/bandejaPaisa.jpg'
    },
    {
      name: 'Arepas',
      description: 'lorem ipsum',
      price: '$5000',
      img: './../../assets/img/comida/arepas.jpg'
    },
    {
      name: 'Arepas Con Guiso',
      description: 'lorem ipsum',
      price: '$7000',
      img: './../../assets/img/comida/arepaGuisp.jpg'
    },
    {
      name: 'Arroz Con Pollo',
      description: 'lorem Ipsum',
      price: '$12000',
      img: './../../assets/img/comida/arrozConPollo.jpg'
    },
    {
      name: 'Empanadas',
      description: 'lorem Ipsum',
      price: '$2000',
      img: './../../assets/img/comida/empanadas.jpeg'
    },
    {
      name: 'Mojarra',
      description: 'lorem Ipsum',
      price: '$17000',
      img: './../../assets/img/comida/mojarra.jpg'
    }
  ];

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  ngOnInit() {
    
  }

  initForm(){
    this.formulario = this.formBuilder.group({
      nombre: '',
      telefono: '',
      direccion: '',
      platos: '',
      cantidad: '',
      fecha: '',
      metodoPago: ''
    });
  }

  mostrarValores(){
    console.log('datos formulario: ', this.formulario.value);
  }
}
